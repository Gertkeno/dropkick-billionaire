#include <gert_Wavefront.h>
#include <gert_Texture.h>
#include <gert_Shader.h>
#include <gert_FontGL.h>
#include <ModelList.h>
#include <cstring>

typedef unsigned char GLubyte;
typedef unsigned int uint;
gert::Wavefront* gModels;
gert::Texture* gTextures;
gert::Shader* gShaders;
gert::Font* gFont;

#ifdef _GERT_DEBUG
#include <iostream>
#endif //_GERT_DEBUG

bool open_assets( void )///< opens all assets
{
	///shaders
	gShaders = new gert::Shader;
	{
		gShaders->open_shader_file( "GLSL/basicObjLighting.vs", "GLSL/basicObjLighting.fs" );
		//vert and frag are char[] with shader files
		/*gShaders->open_shader( vert, gert::Shader::VERTEX );
		gShaders->open_shader( frag, gert::Shader::FRAGMENT );
		gShaders->attach_shaders();*/
		gShaders->use_shader();
	}

	///models
	gModels = new gert::Wavefront[ model::TOTAL ];
	{//baked models
		const float *vert, *tex;
		const uint *face;
		vert = new float[12]{ -1.000000f, -1.000000f, 0.000000f, -1.000000f, 1.000000f, 0.000000f, 1.000000f, -1.000000f, 0.000000f, 1.000000f, 1.000000f, 0.000000f,  };
		tex = new float[8]{ 0.000100f, 0.999900f, 0.999900f, 0.999900f, 0.999900f, 0.000100f, 0.000100f, 0.000100f,  };
		face = new uint[18]{ 1, 0, 0, 3, 1, 0, 2, 2, 0, 1, 0, 0, 2, 2, 0, 0, 3, 0, };
		gModels[ model::PLANE ].obj_load( vert, tex, nullptr, face, 6 );
		delete[] vert; delete[] tex; delete[] face;
	}

	///textures
	gTextures = new gert::Texture[ texture::TOTAL ];
	{///baked textures
		unsigned char foo[] = "\377\0\0\177\377\0\377\177\377\0\377\177\377\0\0\177";
		gTextures[ texture::RED_TEST ].load( foo, 4, 2, 2 );
		unsigned char bar[] = "\377\377\0\177\0\377\377\177\0\377\377\177\377\377\0\177";
		gTextures[ texture::YELLOW_TEST ].load( bar, 4, 2, 2 );

#define BIN_LOAD_SET( t, f )\
		extern void * t##_body_dat asm("_binary_assets_" #t "_body_png_start");\
		extern void * t##_body_size asm("_binary_assets_" #t "_body_png_size");\
		gTextures[ texture:: f##_BODY ].RW_load( & t##_body_dat, (size_t)(& t##_body_size) );\
		extern void * t##_fist_dat asm("_binary_assets_" #t "_fist_png_start");\
		extern void * t##_fist_size asm("_binary_assets_" #t "_fist_png_size");\
		gTextures[ texture:: f##_FIST ].RW_load( & t##_fist_dat, (size_t)(& t##_fist_size) );\
		extern void * t##_head_dat asm("_binary_assets_" #t "_head_png_start");\
		extern void * t##_head_size asm("_binary_assets_" #t "_head_png_size");\
		gTextures[ texture:: f##_HEAD ].RW_load( & t##_head_dat, (size_t)(& t##_head_size) );\
		extern void * t##_foot_dat asm("_binary_assets_" #t "_foot_png_start");\
		extern void * t##_foot_size asm("_binary_assets_" #t "_foot_png_size");\
		gTextures[ texture:: f##_FOOT ].RW_load( & t##_foot_dat, (size_t)(& t##_foot_size) );\

		BIN_LOAD_SET( player, PLAYER );
		BIN_LOAD_SET( grunt, GRUNT );
	}

	///holy error checking
	#ifdef _GERT_DEBUG
	for( GLubyte i = 0; i < model::TOTAL; i++ )
	{
		if( gModels[ i ].get_error_log() == nullptr ) continue;
		std::cout << "model " << int(i) << "\t" << gModels[ i ].get_error_log() << std::endl;
	}
	for( GLubyte i = 0; i < texture::TOTAL; i++ )
	{
		if( gTextures[ i ].get_error_log() == nullptr ) continue;
		std::cout << "texture " << int(i) << "\t" << gTextures[ i ].get_error_log() << std::endl;
	}
	if( gShaders->get_error_log() != nullptr )
	{
		std::cout << gShaders->get_error_log() << std::endl;
	}
	#endif// _GERT_DEBUG

	gFont = new gert::Font;
	{
		extern void *font_dat asm("_binary_assets_coolvetica_rg_ttf_start");
		extern void *font_size asm("_binary_assets_coolvetica_rg_ttf_size");
		//gFont->load_font( "coolvetica rg.ttf", 20 );
		gFont->load_font( &font_dat, (size_t)(&font_size), 20 );
	}
	return true;
}

bool close_assets( void )
{
	delete gFont;
	delete[] gTextures;
	delete[] gModels;
	delete gShaders;
	return true;
}
