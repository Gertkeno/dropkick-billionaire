#include <EvilBase.h>
#include <ModelList.h>
#include <gert_Wavefront.h>
#include <Damage.h>
#include <HurtCircle.h>

namespace evil
{
	EvilBase::EvilBase()
	{
		_animationCycle = 0.0f;
		_active = false;
		_myHurt = nullptr;
		_animeFun = animation::player_walk;
	}

	EvilBase::~EvilBase()
	{
		if( _myHurt == nullptr ) return;
		_myHurt->target = nullptr;
	}

	void EvilBase::update()
	{
		if( health <= 0 or not _active ) return;

		if( _myHurt != nullptr )
		{
			_myHurt->area.pos = pos;
		}
	}

	void EvilBase::start()
	{
		_active = true;
		if( _myHurt != nullptr ) return;
		_myHurt = Damage::get_hurt_circ( this );
	}

	bool EvilBase::get_hurt( int damage, HurtCircle* from )
	{
		if( from == nullptr or from != _myHurt ) return false;
		health -= damage;
		//dead, stop using hurt circle
		if( health <= 0 and _myHurt != nullptr )
		{
			_myHurt->target = nullptr;
			_myHurt = nullptr;
			_active = false;
		}
		return true;
	}

	void EvilBase::apply_force( const geo::point& offset )
	{
		_knockback = offset;
	}

	bool EvilBase::dead()
	{
		return _active and health <= 0;
	}
}
