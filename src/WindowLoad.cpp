#include <WindowLoad.h>
#include <SDL2/SDL_video.h>

namespace wl
{
	SettingsPack open_settings()
	{
		SDL_RWops* sfile = SDL_RWFromFile( "settings.dat", "r" );
		SettingsPack rval;
		rval.sw = 1280;
		rval.sh = 720;
		rval.wflag = SDL_WINDOW_RESIZABLE;
		if( sfile == nullptr ) 
		{
			return rval;
		}
		SDL_RWread( sfile, &rval, sizeof( rval ), 1 );
		SDL_RWclose( sfile );
		return rval;
	}

	bool save_settings(SDL_Window* dat)
	{
		//get values
		SettingsPack total;
		total.wflag = SDL_GetWindowFlags( dat );
		SDL_GetWindowSize( dat, &total.sw, &total.sh );

		//write values
		SDL_RWops* sfile = SDL_RWFromFile( "settings.dat", "w" );
		if( sfile == nullptr ) return false;
		SDL_RWwrite( sfile, &total, sizeof( total ), 1 );
		SDL_RWclose( sfile );
		return true;
	}
}
