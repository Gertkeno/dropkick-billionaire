#include <Geometry.h>
#include <cmath>
#include <algorithm>

//point
geo::point::point()
{
	x = y = 0.0f;
}

geo::point::point( float x, float y )
{
	this->x = x;
	this->y = y;
}

geo::point::point( float b )
{
	this->x = this->y = b;
}

geo::point geo::point::normalize() const
{
	float total = abs(x) + abs(y);
	if( total == 0.0f ) 
		return { 0.0f, 0.0f };
	return { x / total, y / total };
}

geo::point geo::point::ratio() const
{
	float high = std::max( abs(x), abs(y) );
	if( high == 0.0f )
		return { 0.0f, 0.0f };
	return { x/high, y/high };
}

bool geo::point::operator==( const geo::point& comp ) const
{
	return ( x == comp.x and y == comp.y );
}

geo::point geo::point::operator+( const geo::point& a ) const
{
	return geo::point( a.x + x, a.y + y );
}

geo::point geo::point::operator*( const geo::point& a ) const
{
	return geo::point( a.x * x, a.y * y );
}

geo::point geo::point::operator*( const float& a ) const
{
	return geo::point( a * x, a * y );
}

//line
geo::line::line( point a, point b )
{
	this->a = a;
	this->b = b;
}

geo::point geo::line::mid_point() const
{
	return geo::point( (a.x + b.x)/2, (a.y + b.y)/2 );
}

float geo::line::length() const
{
	return sqrt( pow(a.x - b.x, 2) + pow(a.y - b.y, 2) );
}

float geo::line::angle() const
{
	if( a.x - b.x == 0.0f )
		return 0.0f;
	return atan( (a.y - b.y) / (a.x - b.x) );
}

float geo::line::slope() const
{
	if( a.x - b.x == 0.0f ) 
		return 0.0f;
	return (a.y - b.y) / (a.x - b.x);
}

float geo::line::offset() const
{
	return a.y + -(slope() * a.x);
}

float geo::line::get_height( float x ) const
{
	return slope() * x + offset();
}

float geo::line::low_x() const
{
	return std::min( a.x, b.x );
}

float geo::line::high_x() const
{
	return std::max( a.x, b.x );
}

geo::point geo::line::intersection( geo::line pos ) const
{
	float x = (pos.offset() - offset()) / (pos.slope() - slope());
	return geo::point( x, get_height( x ) );
}

//circle
geo::circle::circle()
{
	radius = 0.0f;
}

geo::circle::circle( float x, float y, float r )
{
	pos.x = x;
	pos.y = y;
	this->radius = r;
}

geo::circle::circle( geo::point p, float r )
{
	pos = p;
	radius = r;
}

geo::circle geo::circle::operator=( const geo::point& foo )
{
	pos.x = foo.x;
	pos.y = foo.y;
	return *this;
}

bool geo::circle::operator==( const geo::circle& comp ) const
{
	float totalRadius = this->radius + comp.radius;
	float x = this->pos.x - comp.pos.x;
	float y = this->pos.y - comp.pos.y;

	float dist = sqrt( x*x + y*y );
	return dist < totalRadius;
}

geo::line geo::circle::make_line( float radians ) const
{
	return line( pos, point( cos(radians)*radius, sin(radians)*radius ) );
}

