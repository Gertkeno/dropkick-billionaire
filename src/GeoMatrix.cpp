#include <GeoMatrix.h>
#include <Geometry.h>

namespace geo
{
	glm::mat4 convert_line( const line& in, const float& width )
	{
		glm::mat4 foo = glm::translate( glm::mat4(), glm::vec3( in.mid_point().x, in.mid_point().y, 0.0f ) );
		foo = glm::scale( foo, { width, 1.0f, 1.0f } );
		foo = glm::rotate( foo, in.angle(), { 0.0f, 0.0f, 1.0f } );
		return foo;
	}

	glm::mat4 convert_circle( const circle& in )
	{
		glm::mat4 foo = glm::translate( glm::mat4(), glm::vec3( in.pos.x, in.pos.y, 0.0f ) );
		foo = glm::scale( foo, { in.radius, in.radius, 1.0f } );
		return foo;
	}
}
