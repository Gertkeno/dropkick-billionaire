#include <Damage.h>
#include <vector>
#include <HurtCircle.h>
#include <HitCircle.h>
#ifdef _GERT_DEBUG
#include <iostream>
#include <Layer.h>
#include <GeoMatrix.h>
#include <gert_Wavefront.h>
#include <ModelList.h>
#endif //_GERT_DEBUG

namespace Damage
{
	namespace
	{
		constexpr unsigned HIT_MAX( 300 );
		constexpr unsigned HURT_MAX( 300 );
		HurtCircle* _hurts = new HurtCircle[ HURT_MAX ];
		HitCircle* _hits = new HitCircle[ HIT_MAX ];
	}

	void destroy()
	{
		delete[] _hurts;
		delete[] _hits;
	}

	HurtCircle* get_hurt_circ( Actor* t )
	{
		if( t == nullptr ) return nullptr;
		for( auto i = 0u; i < HURT_MAX; i++ )
		{
			if( _hurts[i].target != nullptr ) continue;
			_hurts[i].target = t;
			return &_hurts[i];
		}
		return nullptr;
	}

	HitCircle* get_hit_circ()
	{
		for( auto i = 0u; i < HIT_MAX; i++ )
		{
			if( _hits[i].get_active() ) continue;
			_hits[i].kill();
			return &_hits[i];
		}
		return nullptr;
	}

	void clear_resources()
	{
		for( auto i = 0u; i < HIT_MAX; i++ )
			_hits[i].kill();

		for( auto i = 0u; i < HURT_MAX; i++ )
			_hurts[ i ].target = nullptr;
	}

	void update()
	{
		for( auto i = 0u; i < HIT_MAX; i++ )
			_hits[i].update();
	}

	void check()
	{
		for( auto u = 0u; u < HURT_MAX; u++ )
		{
			if( _hurts[u].target == nullptr ) continue;
			for( auto i = 0u; i < HIT_MAX; i++ )
			{
				if( not _hits[i].get_active() ) continue;
				_hurts[u].check_collision( &_hits[i] );
			}
		}
	}

#ifdef _GERT_DEBUG
	void draw()
	{
		for( auto i = 0u; i < HURT_MAX; i++ )
		{
			if( _hurts[i].target == nullptr ) continue;
			glm::mat4 a = geo::convert_circle( _hurts[i].area );
			a = glm::translate( a, { 0.0f, 0.0f, layer::top() } );
			gModels[ model::PLANE ].draw( a, texture::YELLOW_TEST, color::WHITE );
		}

		for( auto i = 0u; i < HIT_MAX; i++ )
		{
			if( !_hits[i].get_active() ) continue;
			glm::mat4 a = geo::convert_circle( _hits[i].area );
			a = glm::translate( a, { 0.0f, 0.0f, layer::top() } );
			gModels[ model::PLANE ].draw( a, texture::RED_TEST, color::WHITE );
		}
	}

	std::string used_count()
	{
		std::string dat = "u";
		unsigned hurtTotal = 0u;
		for( auto i = 0u; i < HURT_MAX; i++ )
		{
			if( _hurts[i].target == nullptr ) continue;
			hurtTotal++;
		}
		dat.append( std::to_string( hurtTotal ) );
		dat += '/';
		dat.append( std::to_string( HURT_MAX ) );
		dat.append( " i" );
		unsigned hitTotal = 0u;
		for( auto i = 0u; i < HIT_MAX; i++ )
		{
			if( not _hits[i].get_active() ) continue;
			hitTotal++;
		}
		dat.append( std::to_string( hitTotal ) );
		dat += '/';
		dat.append( std::to_string( HIT_MAX ) );

		return dat;
	}
#endif //_GERT_DEBUG
}// namespace end
