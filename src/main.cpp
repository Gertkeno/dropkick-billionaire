#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>

#include <GameManager.h>
#include <ControlInfo.h>
#include <FrameTime.h>
#include <WindowLoad.h>
#include <random>
#include <ctime>
#include <cstring>

#ifdef _GERT_DEBUG
#include <iostream>
#endif //_GERT_DEBUG

SDL_Window* gWindow;

bool open_assets();
bool close_assets();

int main( int argc, char* argv[] )
{
	ControlInfo::init();
	///SDL INITS
	if( SDL_Init( SDL_INIT_EVERYTHING ) > 0 )
	{
		#ifdef _GERT_DEBUG
		std::cout << "SDL couldn't init\n" << SDL_GetError() << std::endl;
		#endif //_GERT_DEBUG
		return EXIT_FAILURE;
	}
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE );
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, 3 );
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, 2 );
	SDL_GL_SetAttribute( SDL_GL_STENCIL_SIZE, 8 );

	{
		wl::SettingsPack windowDat = wl::open_settings();
		for( int i = 1; i < argc; i++ )//command line arguments
		{
			if( argv[i][0] != '-' ) continue;
			switch( argv[i][1] )
			{
				case 'f':
					if( argc <= i+1 ) break;
					if( strcmp( argv[i+1], "true" ) == 0 )
						windowDat.wflag |= SDL_WINDOW_FULLSCREEN_DESKTOP;
					else if( strcmp( argv[i+1], "false" ) == 0 )
						windowDat.wflag &= ~SDL_WINDOW_FULLSCREEN_DESKTOP;
					break;
				case 'o':
					if( argc <= i+2 ) break;
					{
						const char MIN_SIZE = 9;
						int wval = strtol( argv[i+1], nullptr, 10 );
						int hval = strtol( argv[i+2], nullptr, 10 );
						if( wval > MIN_SIZE and hval > MIN_SIZE )
						{
							windowDat.sw = wval;
							windowDat.sh = hval;
						}
					}
					break;
				case 'b':
					if( argc <= i+1 ) break;
					if( strcmp( argv[i+1], "true" ) == 0 )
					{
						windowDat.wflag |= SDL_WINDOW_BORDERLESS;
						windowDat.wflag &= ~SDL_WINDOW_RESIZABLE;
					}
					else if( strcmp( argv[i+1], "false" ) == 0 )
					{
						windowDat.wflag &= ~SDL_WINDOW_BORDERLESS;
						windowDat.wflag |= SDL_WINDOW_RESIZABLE;
					}
					break;
			}//switch( argv[i][1] )
		}//for arguments
		windowDat.wflag |= SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN;//forced flags
		gWindow = SDL_CreateWindow( "DROPKICK BILLIONAIRE", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, windowDat.sw, windowDat.sh, windowDat.wflag );

		extern void *ico_dat asm("_binary_assets_grunt_head_png_start");
		extern void *ico_size asm("_binary_assets_grunt_head_png_size");
		SDL_Surface *ico = IMG_Load_RW( SDL_RWFromMem( &ico_dat, size_t(&ico_size) ), 1 );
		if( ico != nullptr )
		{
			SDL_SetWindowIcon( gWindow, ico );
			SDL_FreeSurface( ico );
		}
	}
	SDL_GLContext wcontext = SDL_GL_CreateContext( gWindow );

	{
		int flags = IMG_INIT_JPG | IMG_INIT_PNG;
		if( ( IMG_Init( flags )&flags ) != flags )
		{
			#ifdef _GERT_DEBUG
			std::cout << "SDL_image couldn't init\n" << IMG_GetError() << std::endl;
			#endif //_GERT_DEBUG
			return EXIT_FAILURE;
		}
	}
	if( TTF_Init() > 0 )
	{
		#ifdef _GERT_DEBUG
		std::cout << "SDL_ttf couldn't init\n" << TTF_GetError() << std::endl;
		#endif //_GERT_DEBUG
		return EXIT_FAILURE;
	}

	///OpenGL INITS
	glewExperimental = GL_TRUE;
	if( glewInit() > 0 )
	{
		#ifdef _GERT_DEBUG
		std::cout << "GLEW couldn't init\n" << glewGetErrorString( glGetError() ) << std::endl;
		#endif //_GERT_DEBUG
		return EXIT_FAILURE;
	}

	glEnable( GL_DEPTH_TEST );
	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

	GLuint vao;
	glGenVertexArrays( 1, &vao );
	glBindVertexArray(vao);
	{
		int w, h;
		SDL_GetWindowSize( gWindow, &w, &h );
		glViewport( 0, 0, w, h );
	}

	if( SDL_GL_SetSwapInterval( -1 ) != 0 ) //vsync
	{
		#ifdef _GERT_DEBUG
		std::cout << "Late swap not supported.\n";
		#endif //_GERT_DEBUG
		SDL_GL_SetSwapInterval( 1 );
	}

	///Assets INIT
	open_assets();

	///actual code lol
	srand( time( nullptr ) );
	GameManager::init();

	short int joyMax = SDL_NumJoysticks();
	SDL_Joystick** joys = new SDL_Joystick*[joyMax];
	for( short int i = 0; i < joyMax; i++ )
	{
		joys[ i ] = SDL_JoystickOpen( i );
	}
	if( not ControlInfo::open_file( "control.ini" ) )
	{
		ControlInfo::parse_file( R"(CONFIRM <Z> 0
UP <Up> 1
DOWN <Down> 2
LEFT <Left> 3
RIGHT <Right> 4
SLOWMO <Left Shift>
PUNCH <X> 5
DROP_KICK ~ 0)" );
	}

	static constexpr float MAX_FPS = 1000.0f/90.0f;

	while( GameManager::get_game_state() != GameManager::gmsQUITTING )
	{
		FrameTime::update();

		GameManager::update();
		GameManager::draw();

		if( FrameTime::since_update() < MAX_FPS )
		{
			SDL_Delay( MAX_FPS - FrameTime::since_update() );
		}
	}

	///deinit
	if( wl::save_settings( gWindow ) )
	{
		#ifdef _GERT_DEBUG
		std::cout << "Window data saved\n";
		#endif // _GERT_DEBUG
	}
	ControlInfo::save_controls( "control.ini" );
	for( short int i = 0; i < joyMax; i++ )
	{
		if( SDL_JoystickGetAttached( joys[i] ) ) SDL_JoystickClose( joys[i] );
	}
	delete[] joys;
	close_assets();
	ControlInfo::destroy();
	GameManager::destroy();
	SDL_GL_DeleteContext( wcontext );
	SDL_DestroyWindow( gWindow );
	SDL_Quit();
	IMG_Quit();
	TTF_Quit();
	return EXIT_SUCCESS;
}
