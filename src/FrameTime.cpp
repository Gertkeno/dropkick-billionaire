#include <FrameTime.h>
#include <SDL2/SDL_timer.h>

namespace FrameTime
{
	namespace
	{
		double _pureTime;
		double _modded;
		double _modBuffer(1.0);
		unsigned _frameStart(0);

		constexpr float UNIT_CONVERT = 1000.0f;
	}

	double get_pure()
	{
		return _pureTime;
	}

	double get_mod()
	{
		return _modded;
	}

	void set_modifier( double set )
	{
		_modBuffer = set;
	}

	void update()
	{
		_pureTime = since_update() / UNIT_CONVERT;///1000 ms in one second this converts _pureTime to the unit seconds
		_modded = _pureTime * _modBuffer;
		_frameStart = SDL_GetTicks();
	}

	unsigned since_update()
	{
		return (SDL_GetTicks() - _frameStart);
	}
}
