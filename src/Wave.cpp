#include <Wave.h>
#include <algorithm>
#include <random>
#include <Grunt.h>
#include <FrameTime.h>

namespace Wave
{
	namespace
	{
		constexpr float ENEMY_START_TIME{ 0.8f };

		int _iteration{0};
		evil::EvilBase **_troops{ nullptr };
		unsigned _totalTroops{0u};
		float _elapsedTime;
		unsigned _starter;
	}

	void update()
	{
		if( _troops == nullptr ) return;
		_elapsedTime += FrameTime::get_mod();
		for( auto i = 0u; i < _totalTroops; i++ )
		{
			_troops[i]->update();
		}

		if( _elapsedTime - _starter*ENEMY_START_TIME > 0.0f )
		{
			if( _starter < _totalTroops ) _troops[_starter]->start();
			_starter++;
		}
	}

	void init()
	{
		_starter = 0u;
		_elapsedTime = 0.0f;
		if( _troops != nullptr )
		{
			destroy();
		}
		_totalTroops = _iteration*2 + 1;
		_troops = new evil::EvilBase*[_totalTroops];
		unsigned hat = rand() % std::min( _totalTroops, 50u );
		for( auto i = 0u; i < _totalTroops; i++ )
		{
			_troops[ i ] = new evil::Grunt();
			//post troop gen
			if( hat == i )
			{
			}
		}
	}

	void destroy()
	{
		for( auto i = 0u; i < _totalTroops; i++ )
		{
			delete _troops[i];
		}
		if( _troops != nullptr )
		{
			delete[] _troops;
			_troops = nullptr;
		}
		_totalTroops = 0u;
	}
		

	void draw_enemies()
	{
		if( _troops == nullptr ) return;
		for( auto i = 0u; i < _totalTroops; i++ )
		{
			_troops[i]->draw();
		}
	}

	void increment_total( int val ) 
	{
		if( _iteration + val < 0 ) 
		{
			_iteration = 0;
			return;
		}
		_iteration += val;
	}

	bool all_dead()
	{
		if( _troops != nullptr ) return false;
		for( auto i = 0u; i < _totalTroops; i++ )
		{
			if( not _troops[i]->dead() ) return false;
		}
		return true;
	}
}
