#include <Animation.h>
#include <gert_Wavefront.h>
#include <glm/gtc/matrix_transform.hpp>
#include <ModelList.h>
#include <cmath>

namespace animation
{
	Data Data::operator+( const geo::point& a )
	{
		Data temp;
		temp.head = head + a;
		temp.leftHand = leftHand + a;
		temp.rightHand = rightHand + a;
		temp.leftFoot = leftFoot + a;
		temp.rightFoot = rightFoot + a;
		return temp;
	}

	Data Data::operator*( const geo::point& a )
	{
		Data temp;
		temp.head = head * a;
		temp.leftHand = leftHand * a;
		temp.rightHand = rightHand * a;
		temp.leftFoot = leftFoot * a;
		temp.rightFoot = rightFoot * a;
		return temp;
	}

	Data Data::operator*( const float& a )
	{
		Data temp;
		temp.head = head * a;
		temp.leftHand = leftHand * a;
		temp.rightHand = rightHand * a;
		temp.leftFoot = leftFoot * a;
		temp.rightFoot = rightFoot * a;
		return temp;
	}

	Data::Data()
	{
		angle = 0.0f;
	}

	void draw_anime( const geo::point& ani, float rot, int texture, float layer, geo::point size )
	{
		glm::mat4 foo = glm::translate( glm::mat4(), { ani.x, ani.y, layer } );
		foo = glm::scale( foo, { size.x, size.y, 1.0f } );
		foo = glm::rotate( foo, rot, { 0.0f, 0.0f, 1.0f } );
		gModels[ model::PLANE ].draw( foo, texture, color::WHITE );
	}

	Data player_walk( float time )
	{
		Data rval;
		rval.angle = 0.0f;
		const float radTime = time*2*M_PI;
		float s = sin( radTime );
		float c = cos( radTime );

		rval.head = { 0.0f, 0.07f*s + 1.0f };
		rval.leftFoot = { 0.4f*c-0.2f, 0.4f*s-0.8f };
		rval.rightFoot = { -0.4f*c+0.2f, -0.4f*s-0.8f };
		rval.leftHand = { 0.25f*c-0.6f, 0.3f*(-s*s)+0.35f };
		rval.rightHand = { 0.25f*s+0.6f, 0.3f*(-c*c)+0.35f };
		return rval;
	}

	Data player_fall( float time )
	{
		Data rval;
		rval.angle = 0.0f;
		const float radTime = time*2*M_PI;
		float s = sin( radTime*2 );
		float c = cos( radTime );
		
		rval.head = { 0.0f, 1.0f };
		rval.leftFoot = { 0.6f+c*0.04f, -1.0f };
		rval.rightFoot = { -0.6f-c*0.04f, -1.0f };
		rval.leftHand = { 0.6f+c*0.05f, 0.3f*s+0.6f };
		rval.rightHand = { -0.6f+c*0.05f, 0.3f*-s+0.6f };
		return rval;
	}
}
