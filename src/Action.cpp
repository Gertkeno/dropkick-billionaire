#include <Action.h>

Action::Action()
{
	//ctor
	_active = false;
	_timer = 0.0f;
	angle = 0.0f;
	_owner = nullptr;
	_hitConfirm = false;
}

Action::~Action()
{
	//dtor
}

bool Action::get_active()
{
	return _active;
}

void Action::kill()
{
	_active = false;
}

void Action::draw()
{

}

bool Action::hit_confirm()
{
	return _hitConfirm;
}
