#include <Player.h>

#include <glm/gtc/matrix_transform.hpp>
#include <gert_Wavefront.h>
#include <gert_Shader.h>
#include <ControlInfo.h>
//#include <GameManager.h>
#include <Damage.h>
#include <HurtCircle.h>
#include <Action.h>
#include <Moveset_Player.h>
#include <ModelList.h>
#include <FrameTime.h>
#include <Stage.h>

static constexpr float HEIGHT = 0.4f;
static constexpr float ANIMATION_TIME_MAX = 1.0f;
static constexpr float STAND_WIDTH = 0.2f;
static constexpr float FREE_FALL_TIME = 0.28f;
static constexpr float JUMP_UP = 8.0f;
static constexpr float MOVE_SPEED = 1.86f;

#ifdef _GERT_DEBUG
#include <iostream>
#endif //_GERT_DEBUG

Player::Player( void )
{
	health = 10;
	_yspeed = 0.0f;
	state = WALK;
	_offGroundTimer = 0.0f;
	_animationCycle = 0.0f;
	_currentAction = nullptr;
	_myHurt = nullptr;
	_animeFunc = animation::player_walk;
	_facingRight = true;
}

Player::~Player( void )
{
	if( _myHurt != nullptr )
	{
		_myHurt->target = nullptr;
	}
	remove_action();
}

void Player::update()
{
	if( _myHurt == nullptr )
	{
		_myHurt = Damage::get_hurt_circ( this );
	}
	_offGroundTimer += FrameTime::get_mod();
	switch( state )
	{
		case TOTAL:
		case ACTION:
		case FALL:
			_animationCycle += FrameTime::get_mod();
			break;
		case DEBUG:
		case WALK:
			if( CI_IN( LEFT ) ) _animationCycle += FrameTime::get_mod();
			else if( CI_IN( RIGHT ) ) _animationCycle -= FrameTime::get_mod();
			break;
	}

	if( _animationCycle > ANIMATION_TIME_MAX ) _animationCycle = 0.0f;
	else if( _animationCycle < 0.0f ) _animationCycle = ANIMATION_TIME_MAX;

	if( _animeFunc != nullptr )
	{
		_bodyPart = _animeFunc( _animationCycle/ANIMATION_TIME_MAX );
		_bodyPart = _bodyPart * HEIGHT;
		_bodyPart = _bodyPart + geo::point( _pos.x, _pos.y + HEIGHT );
	}

	if( CI_IN(SLOWMO) )
	{
		FrameTime::set_modifier( 0.3 );
	}
	else
	{
		FrameTime::set_modifier( 1.0 );
	}

	if( state != ACTION )
	{
		if( CI_IN( DROP_KICK ) )
		{
			if( _currentAction == nullptr )
			{
				//_currentAction = new moveset::normal::DropKick( _bodyPart, this );
				_currentAction = new moveset::normal::Punch( _bodyPart, this );
			}
			_change_state( ACTION );
		}
		else if( CI_IN( LEFT ) )
		{
			_facingRight = false;
			_pos.x -= MOVE_SPEED * FrameTime::get_mod();
		}
		else if( CI_IN( RIGHT ) )
		{
			_facingRight = true;
			_pos.x += MOVE_SPEED * FrameTime::get_mod();
		}
	}
	else// state == action
	{
		if( _currentAction == nullptr ) 
			_change_state( FALL );
		else
		{
			_currentAction->position = _bodyPart;
			if( _facingRight )
				_currentAction->angle = 0;
			else
				_currentAction->angle = M_PI;
			if( _currentAction->update() )
			{
				_change_state( FALL );
			}
		}
	}

	if( state != FALL and _offGroundTimer < FREE_FALL_TIME )
	{
		if( CI_IN( UP ) )
		{
			_change_state( FALL );
			_yspeed = JUMP_UP;
		}
	}

	//applying speed
	{
		float gravMod = GRAVITY;
		switch( state )
		{
			case WALK:
			case FALL:
			case DEBUG:
			case TOTAL:
				break;
			case ACTION:
				gravMod = GRAVITY/8.0f;
				break;
		}//switch( state )
		_yspeed -= gravMod * FrameTime::get_mod();
		_pos.y += _yspeed * FrameTime::get_mod();
	}

	//checking for ground
	geo::circle checker( _pos, STAND_WIDTH );
	geo::point force = Stage::stage_force( checker );
	//std::cout << "Stage y force: " << force.y << std::endl;
	if( force.y > -0.03f )
	{
		if( state != ACTION ) _change_state( WALK );
		_offGroundTimer = 0.0f;
		_yspeed = 0;
	}
	else if( state == WALK and _offGroundTimer > FREE_FALL_TIME )
	{
		_change_state( FALL );
	}
	if( force.y < -0.03f )
	{
		force.y = 0;
	}
	_pos = _pos + force;
	if( _myHurt != nullptr )
	{
		_myHurt->area.pos = _pos;
		_myHurt->area.pos.y += HEIGHT;
		_myHurt->area.radius = 0.26f;
	}
}

void Player::draw()
{
	/*ENTIRE FIGURE*/
	glm::mat4 model;
	if( _facingRight )
		gert::Shader::Active_Shader->set_uniform( "flip", false );
	else
		gert::Shader::Active_Shader->set_uniform( "flip", true );

	/*BODY*/
	static const geo::point BODY_SIZE( 0.17f, 0.22f );
	animation::draw_anime( {_pos.x, _pos.y+HEIGHT}, _bodyPart.angle, texture::PLAYER_BODY, layer::top(), BODY_SIZE );
	/*FEET*/
	static const geo::point FOOT_SIZE( 0.13f, 0.08f );
	float footAngle{ 0.0f };
	if( state == WALK )
	{
		geo::line temp;
		temp.a = _bodyPart.leftFoot; temp.b = _bodyPart.rightFoot;
		temp = Stage::drape_floor( temp );
		footAngle = temp.angle();
		if( _bodyPart.leftFoot.y < temp.a.y ) _bodyPart.leftFoot = temp.a;
		if( _bodyPart.rightFoot.y < temp.b.y ) _bodyPart.rightFoot = temp.b;
	}
	footAngle += _bodyPart.angle;
	animation::draw_anime( _bodyPart.leftFoot, footAngle, texture::PLAYER_FOOT, layer::top(), FOOT_SIZE );
	animation::draw_anime( _bodyPart.rightFoot, footAngle, texture::PLAYER_FOOT, layer::top(), FOOT_SIZE );
	/*HAND*/
	static const geo::point HAND_SIZE( 0.08f );
	animation::draw_anime( _bodyPart.leftHand, _bodyPart.angle, texture::PLAYER_FIST, layer::top(), HAND_SIZE );
	animation::draw_anime( _bodyPart.rightHand, _bodyPart.angle, texture::PLAYER_FIST, layer::top(), HAND_SIZE );
	/*HEAD*/
	static const geo::point HEAD_SIZE( 0.2f );
	animation::draw_anime( _bodyPart.head, _bodyPart.angle, texture::PLAYER_HEAD, layer::top(), HEAD_SIZE );

	gert::Shader::Active_Shader->set_uniform( "flip", false );
}

bool Player::get_hurt( int damage, HurtCircle* from )
{
	health -= damage;
	return true;
}

void Player::_change_state( state_t foo )
{
	if( state == foo ) return;
	_animationCycle = 0.0f;
	if( state == ACTION )
	{
		remove_action();
	}
	switch( foo )
	{
		case WALK:
			_animeFunc = animation::player_walk;
			break;
		case ACTION:
			_animeFunc = animation::player_fall;
			_yspeed = 0;
			break;
		case FALL:
			_animeFunc = animation::player_fall;
			_offGroundTimer = FREE_FALL_TIME;
			break;
		default:
			break;
	}

	state = foo;
}

void Player::remove_action()
{
	if( _currentAction == nullptr ) return;
	delete _currentAction;
	_currentAction = nullptr;
}

void Player::apply_force( const geo::point& offset)
{
	_pos.x += offset.x;
	_pos.y += offset.y;
}

geo::point Player::get_pos()
{
	return _pos;
}
