#include <HurtCircle.h>
#include <HitCircle.h>

#include <Geometry.h>
#include <Actor.h>
#ifdef _GERT_DEBUG
#include <iostream>
#endif //_GERT_DEBUG

HurtCircle::HurtCircle()
{
    //ctor
    target = nullptr;
}

HurtCircle::~HurtCircle()
{
    //dtor
	//delete area;
}

bool HurtCircle::check_collision( HitCircle* hitc )
{
    ///  check if both are active.                    source is not the same       collision check
    if( ( target != nullptr and hitc->get_active() ) and target != hitc->source and hitc->area == area )
    {
        if( target->get_hurt( hitc->damage, this ) )
        {
			if( target != nullptr ) target->apply_force( hitc->force );
            hitc->confirmHit = true;
        }
        return true;
    }
    return false;
}

HurtCircle HurtCircle::operator=( const geo::circle& foo )
{
	HurtCircle temp( *this );
	temp.area = foo;
	return temp;
}
