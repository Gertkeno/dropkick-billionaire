#include <Stage.h>
#include <vector>
#include <cmath>
#include <gert_Wavefront.h>
#include <ModelList.h>

#include <iostream>
namespace Stage
{
	namespace //anon namespace for private variables
	{
		std::vector<geo::point> _floor;
		std::vector<geo::point> _wall;

		glm::mat4 *_model{ new glm::mat4 };
		int _texture;
	}

	//single point draping, returns y value of floor, if not aligned with floor returns pos.y
	float drape_floor( const geo::point& pos )
	{
		for( auto i = 0u; i < _floor.size() - 1u; i++ )
		{
			geo::line ctFloor( _floor[ i ], _floor[ i + 1 ] );
			if( pos.x < ctFloor.low_x() or pos.x > ctFloor.high_x() ) continue;
			return ctFloor.get_height( pos.x );
		}
		return pos.y;//not on stage
	}

	//converts input to line with y values draped
	geo::line drape_floor( const geo::point& one, const geo::point& two )
	{
		geo::point foo{ one.x, drape_floor( one ) };
		geo::point bar{ two.x, drape_floor( two ) };
		return geo::line( foo, bar );
	}

	geo::line drape_floor( const geo::line& pos )
	{
		return drape_floor( pos.a, pos.b );
	}

	//returns least required force to move collidable (pos) out of walls
	float wall_force( geo::line pos )
	{
		for( auto &i: _wall )
		{
			if( i.x >= pos.high_x() or i.x <= pos.low_x() ) continue; //continue where no collision on X axis
			float ctHeight = drape_floor( i ) + i.y;//temporary for wall height plus floor height
			if( pos.a.y > ctHeight or pos.b.y > ctHeight ) continue;

			float l = ( i.x - pos.low_x() );
			float h = ( i.x - pos.high_x() );
			//absolute values for comparisons
			float al = std::abs(l);
			float ah = std::abs(h);

			if( al < ah )
				return l;
			else
				return h;
		}
		return 0.0f;
	} 

	float wall_force( const geo::circle& foo )
	{
		geo::point left( foo.pos.x - foo.radius, foo.pos.y );
		geo::point right( foo.pos.x + foo.radius, foo.pos.y );
		return wall_force( geo::line( left, right ) );
	}

	float wall_force( const geo::point& left, const geo::point& right )
	{
		return wall_force( geo::line( left, right ) );
	}

	//hard coded stage data. floors, walls, and texture.
	bool load_stage( files type )
	{
		_texture=0;
		_floor.clear();
		_wall.clear();

		if( type >= TOTAL or type < 0 )
			return false;
		switch( type )
		{
			case MOUNTAIN:
				_floor = std::vector<geo::point>( { { 3, 0 }, { 0, -0.9f }, { -1, -0 } } );
				_wall.push_back( geo::point( -0.5f, 0.7f ) );
				*_model = glm::mat4();
				return true;
			case TOTAL:
				break;
		}
		return false;
	}

	void draw()
	{
		gModels[ model::PLANE ].draw( *_model, _texture, color::WHITE );
	}

	//makes a vector of least resistant to move collidable (foo) out of floors and walls
	geo::point stage_force( const geo::circle& foo )
	{
		geo::point force;
		force.x = wall_force( foo );

		for( auto i = 0u; i < _floor.size() - 1u; i++ )
		{
			geo::line ctFloor( _floor[ i ], _floor[ i + 1 ] );
			if( ctFloor.low_x() > foo.pos.x or ctFloor.high_x() < foo.pos.x ) continue;
			float h = ctFloor.get_height( foo.pos.x );
			force.y = h - (foo.pos.y - foo.radius);
			break;
		}

		return force;
	}
}//namespace Stage
