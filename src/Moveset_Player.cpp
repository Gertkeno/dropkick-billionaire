#include <Moveset_Player.h>
#include <FrameTime.h>
#include <Damage.h>
#include <HitCircle.h>
#include <cmath>

namespace moveset
{
	using namespace normal;
	DropKick::DropKick( const animation::Data& pos, Actor *owner )
	{
		position = pos;
		gotHit = 0.0f;
		_lastHit = Damage::get_hit_circ();
		_active = true;
		this->_owner = owner;
	}

	DropKick::~DropKick()
	{
		//FrameTime::set_modifier( 1.0 );
	}

	bool DropKick::update()
	{
		_timer += FrameTime::get_mod();
		if( gotHit > 0.0f )
		{
			gotHit += FrameTime::get_mod();
		}
		else if( _lastHit != nullptr and _lastHit->confirmHit )
		{
			gotHit += FrameTime::get_mod();
			//FrameTime::set_modifier( 0.6 );
		}
		else
		{
			//init hit
			geo::circle foo; foo = position.leftHand;
			static constexpr float DISTANCE{ 0.4f };
			foo.pos.x += cos( angle ) * DISTANCE;
			foo.radius = 0.6f;
			_lastHit->start( 0, _owner, foo );
		}
		if( _timer > MAX_TIME and ( gotHit == 0.0f or gotHit > MAX_TIME ) )
		{
			return not (_active = false);
		}
		return false;
	}

	float DropKick::get_timer()
	{
		return _timer/MAX_TIME;
	}

	Punch::Punch( const animation::Data& pos, Actor* owner )
	{
		position = pos;
		_active = true;
		this->_owner = owner;
		_lastHit = nullptr;
	}

	Punch::~Punch()
	{
	}

	bool Punch::update()
	{
		_timer += FrameTime::get_mod();
		if( _lastHit != nullptr and _lastHit->confirmHit )
		{
			_hitConfirm = true;
		}
		_lastHit = Damage::get_hit_circ();
		geo::circle spot;
		spot.radius = 1.0f;
		spot.pos = position.leftHand;
		_lastHit->start( 1, _owner, spot );
		if( angle > 0 ) _lastHit->force = geo::point( 0.2f );
		else _lastHit->force = geo::point( -0.2f );
		if( _timer > MAX_TIME ) return true;
		return false;
	}

	float Punch::get_timer()
	{
		return _timer;
	}
}
