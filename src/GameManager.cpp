#include <GameManager.h>

///Library includes
#include <SDL2/SDL_events.h>
#include <SDL2/SDL_timer.h>
#include <GL/glew.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <string>
#include <FrameTime.h>
#include <Damage.h>

///Game Data
#include <gert_Shader.h>
#include <HurtCircle.h>
#include <ModelList.h>
#include <HitCircle.h>
#include <ControlInfo.h>
#include <gert_GLCamera.h>
#include <Player.h>
#include <Stage.h>
#include <Wave.h>

///Drawing headers
#include <gert_FontGL.h>
#include <gert_Wavefront.h>
#include <ModelList.h>
#include <gert_GLmath.h>
#include <gert_Texture.h>
#ifdef _GERT_DEBUG
#include <iostream>
#define DE_OUT( x ) std::cout << "[GameManager]  " << x << std::endl
#endif // _GERT_DEBUG

namespace GameManager
{
	namespace//private member declarations
	{
		//constants
		constexpr float HIDE_CURSOR_TIME = 1.56f;

		//management
		state _gameState;
		SDL_Event* _event;
		glm::mat4* _view;
		glm::mat4* _proj;
		Player* _player;
		int _locView;

		gert::Camera* _cam;

		///game data

		///UI options screen
		float _idleCursor;
	}

	void _update_shader( bool calcProj = false );

	void init( void )
	{
		_idleCursor = 0.0f;
		_view = new glm::mat4;
		_proj = new glm::mat4;

		_event = new SDL_Event;
		_player = new Player;

		_cam = new gert::Camera;
		_cam->above = { 0.0f, 1.0f, 0.0f };
		_cam->position = glm::vec3( 0.0f );
		_cam->front = { 0.0f, 0.0f, -1.0f };

		Stage::load_stage( Stage::MOUNTAIN );

		_update_shader( true );
		change_game_state( gmsPLAYING );
	}

	void destroy( void )
	{
		Damage::destroy();
		Stage::load_stage( Stage::files(-1) );
		delete _event;
		delete _view;
		delete _proj;

		delete _player;

		delete _cam;
	}

	void update( void )
	{
		///GLOBAL ACTIONS
		ControlInfo::frame_reset();
		_idleCursor += FrameTime::get_pure();
		while( SDL_PollEvent( _event ) )
		{
			switch( _event->type )//global events
			{
				case SDL_KEYDOWN:
					switch( _event->key.keysym.sym )
					{
						case SDLK_F1:
							Wave::increment_total();
							Wave::init();
							break;
					}
					break;
				case SDL_MOUSEMOTION:
					_idleCursor = 0.0f;
					SDL_ShowCursor( SDL_ENABLE );
					break;
				case SDL_QUIT:
					_gameState = gmsQUITTING;
					break;
				case SDL_WINDOWEVENT:
					switch( _event->window.event )
					{
						case SDL_WINDOWEVENT_RESIZED:
							_update_shader( true );
							break;
					}
					break;
			}

			///polling for controls
			ControlInfo::manage_events( *_event );//have to poll this first
			switch( _gameState )//gamestate events
			{
				case gmsPLAYING:
					if( ControlInfo::get_pause_tick() )
					{
						//change_game_state( gmsTITLE_SCREEN );
					}
					break;
				case gmsQUITTING:
					break;
			}
		}//SDL_PollEvent( _event )

		if( _idleCursor > 1.56f )//cursor hider
		{
			SDL_ShowCursor( SDL_DISABLE );
			_idleCursor = -999.0f;
		}

		///UPDATING OBJECTS
		switch( _gameState )
		{
			case gmsPLAYING:
				{
					Damage::update();

					_player->update();
					Wave::update();

					//run collision checks
					Damage::check();
				}
				break;
			case gmsQUITTING:
				break;
		}
	}

	void draw( void )
	{
		glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
		layer::reset();

		///DRAWS
		if( _gameState == gmsPLAYING )
		{
			///PLAYERS AND WHAT NOT
			//Stage::draw();
			Wave::draw_enemies();
			_player->draw();

			#ifdef _GERT_DEBUG
			Damage::draw();
			//static constexpr auto devText =  "how about I tell you a tale of really long text to test wrapping""dev build please ignore, thanks.";
			static constexpr float hSize = 0.12f;
			gFont->draw( &gModels[ model::PLANE ], Damage::used_count(), color::WHITE, glm::vec2( -1.0f + hSize, 1.0f - hSize ), hSize, layer::top(), 0.04f );
			#endif //_GERT_DEBUG
		}
		SDL_GL_SwapWindow( gWindow );
	}

	void change_game_state( state ct )
	{
		ControlInfo::frame_reset();
		_gameState = ct;

		switch( ct )///shaders set
		{
			case gmsPLAYING:
				//SDL_ShowCursor( SDL_DISABLE );
				break;
				/*case gmsCONTROLLER_CONFIG:
				  case gmsKEYBOARD_CONFIG:*/
				/*case gmsTITLE_SCREEN:
				  break;*/
			case gmsQUITTING:
				break;
		}
	}

	#define EASY_WRITE SDL_RWwrite( sfile, &prefix, sizeof( char ), 1 ); SDL_RWwrite( sfile, &size, sizeof( uint16_t ), 1 );
	void save_game( void )
	{
		#ifdef _GERT_DEBUG
		//std::cout << "[GameManager][save_game]  Save written\n";	
		#endif //_GERT_DEBUG
	}

	void _update_shader( bool calcProj )
	{
		GLuint prog = gert::Shader::Active_Shader->get_program();
		_locView = glGetUniformLocation( prog, "view" );
		*_view = glm::lookAt( _cam->position, _cam->front, _cam->above );
		glProgramUniformMatrix4fv( prog, _locView, 1, GL_FALSE, glm::value_ptr( *_view ) );
		gert::Shader::Active_Shader->set_uniform( "shading", 1.0f );
		gert::Shader::Active_Shader->set_uniform( "sheetFrame", 0u );
		gert::Shader::Active_Shader->set_uniform( "flip", false );
		gert::Wavefront::update_attribs();

		if( calcProj )
		{
			int w, h;
			SDL_GetWindowSize( gWindow, &w, &h );
			glViewport( 0, 0, w, h );
			const geo::point foo( geo::point( w, h ).ratio() );
			*_proj = glm::scale( glm::mat4(), { foo.y, foo.x, 1.0f } );
		}
		GLint uniProj = glGetUniformLocation(prog, "proj");
		glProgramUniformMatrix4fv( prog, uniProj, 1, GL_FALSE, glm::value_ptr(*_proj) );
		glUniformMatrix4fv( _locView, 1, GL_FALSE, glm::value_ptr( *_view ) );
	}

	state get_game_state()
	{
		return _gameState;
	}

	geo::point get_player_pos()
	{
		return _player->get_pos();
	}
}
