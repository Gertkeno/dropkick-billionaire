#include <Layer.h>

/*
 * when drawing flat perspective 2D z value has to be between 0 and 1.
 * top() will go through values from 1.0 to inf, decrementing slowly.
 * call reset() at the start of a frame to restart from 1.0
 */

namespace layer
{
	static float _current;

	void reset()
	{
		_current = 1.0f;
	}

	float top()
	{
		_current -= 0.0000005f;
		return _current;
	}
}
