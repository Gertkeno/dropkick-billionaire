#include "HitCircle.h"

#include <Geometry.h>
#include <FrameTime.h>

HitCircle::HitCircle()
{
    //ctor
    source = nullptr;
    _lifeTime = 999.9f;
    maxTime = 0.0f;
	confirmHit = false;
}

HitCircle::~HitCircle()
{
    //dtor
}

bool HitCircle::get_active( void )
{
    return _lifeTime <= maxTime and source != nullptr;
}

void HitCircle::update( void )
{
    _lifeTime += FrameTime::get_mod();
}

void HitCircle::start( int damage, Actor* from, const geo::circle& pos )
{
    _lifeTime = 0.0f;
    maxTime = 0.0f;
    this->damage = damage;
    this->source = from;
    this->area = pos;
	force = geo::point(0.0f);
    confirmHit = false;
}

void HitCircle::kill( void )
{
    _lifeTime = maxTime+1.0f;
	confirmHit = false;
}
