#include <Grunt.h>
#include <FrameTime.h>
#include <random>
#include <ModelList.h>
#include <Stage.h>
#include <gert_Shader.h>
#include <HurtCircle.h>
#include <GameManager.h>

namespace evil
{
	namespace
	{
		constexpr float TURN_RATE( 1.8f );
		constexpr float ANIME_TIME( 1.1f );
		constexpr float MOVE_SPEED( 1.444442f );
		constexpr float HEIGHT( 0.3f );
		constexpr float HIT_DISTANCE( 0.8f );
		constexpr float STAND_WIDTH( 0.2f );
	}

	Grunt::Grunt()
	{
		_attacking = false;
		_moveUnit = 0;
		_lastTurn = _lastAttack = 0.0f;
	}

	Grunt::~Grunt()
	{
	}

	void Grunt::draw()
	{
		if( not _active ) return;
		if( int(_hitStun*1000)%200 > 100 ) return;
		if( _moveUnit < 0 ) gert::Shader::Active_Shader->set_uniform( "flip", true );
		static const geo::point BODY_SIZE( 0.09f, 0.16f );
		animation::draw_anime( { pos.x, pos.y+HEIGHT }, _bodyParts.angle, texture::GRUNT_BODY, layer::top(), geo::point(BODY_SIZE) );
		static const geo::point HEAD_SIZE( 0.13f, 0.16f);
		animation::draw_anime( _bodyParts.head, _bodyParts.angle, texture::GRUNT_HEAD, layer::top(), geo::point(HEAD_SIZE) );
		static const geo::point FOOT_SIZE( 0.09f, 0.05f );
		animation::draw_anime( _bodyParts.leftFoot, _bodyParts.angle, texture::GRUNT_FOOT, layer::top(), geo::point(FOOT_SIZE) );
		animation::draw_anime( _bodyParts.rightFoot, _bodyParts.angle, texture::GRUNT_FOOT, layer::top(), geo::point(FOOT_SIZE) );
		static const geo::point HAND_SIZE( 0.08f );
		animation::draw_anime( _bodyParts.leftHand, _bodyParts.angle, texture::GRUNT_FIST, layer::top(), geo::point(HAND_SIZE) );
		animation::draw_anime( _bodyParts.rightHand, _bodyParts.angle, texture::GRUNT_FIST, layer::top(), geo::point(HAND_SIZE) );
		gert::Shader::Active_Shader->set_uniform( "flip", false );
	}

	void Grunt::update()
	{
		if( not _active ) return;
		_hitStun -= FrameTime::get_mod();

		//get animation points
		if( _animeFun != nullptr )
		{
			_bodyParts = _animeFun( _animationCycle/ANIME_TIME );
			_bodyParts = _bodyParts * HEIGHT;
			_bodyParts = _bodyParts + geo::point( pos.x, pos.y+HEIGHT );
		}

		if( not _attacking and _hitStun < 0.0f )//movement
		{
			_lastTurn += FrameTime::get_mod();
			_lastAttack += FrameTime::get_mod();
			if( _lastTurn > TURN_RATE )
			{
				_moveUnit = rand()%3 - 1;
				_lastTurn = (rand()%1000)/1000.0f;
			}

			if( _moveUnit < 0 ) _animationCycle += FrameTime::get_mod();
			else if( _moveUnit > 0 ) _animationCycle -= FrameTime::get_mod();
			if( _animationCycle > ANIME_TIME ) _animationCycle = 0.0f;
			else if( _animationCycle < 0.0f ) _animationCycle = ANIME_TIME;

			pos.x += MOVE_SPEED * _moveUnit * FrameTime::get_mod();
			pos.y -= GRAVITY * FrameTime::get_mod();
			pos = pos + Stage::stage_force( { pos, STAND_WIDTH } );
			
			//get player distance, set attacking if short
			if( geo::line( pos, GameManager::get_player_pos() ).length() < HIT_DISTANCE )
			{

			}
		}
		else if( _attacking )
		{
			//get if player is close enough and do an attack, assuming last attack what a while ago
		}
		else
		{
			//hitstun
			pos = pos + _knockback*FrameTime::get_mod();
		}

		//setting hurt data
		if( _myHurt != nullptr )
		{
			_myHurt->area.pos = { pos.x, pos.y + HEIGHT };
			_myHurt->area.radius = HEIGHT;
		}
	}

	void Grunt::start()
	{
		EvilBase::start();
		_hitStun = 0.0f;
		health = 3;
	}

	bool Grunt::get_hurt( int damage, HurtCircle* from )
	{
		if( _hitStun > 0.0f ) return false;
		bool fhit = EvilBase::get_hurt( damage, from );
		if( fhit )
		{
			_attacking = false;
			_hitStun = 0.67f;
		}
		return fhit;
	}
}
