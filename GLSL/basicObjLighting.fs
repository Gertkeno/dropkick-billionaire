#version 330 core

in vec2 TexCoord;

uniform uint sheetWidth;
uniform uint sheetHeight;
uniform uint sheetFrame;

uniform float shading;
uniform int flip;

uniform sampler2D tex0;
uniform sampler2D tex1;
uniform float overa;
uniform vec4 colorMod;

void main()
{
	vec4 col0;
	vec2 ftc;
	ftc.x = (TexCoord.x + sheetFrame%sheetWidth) * 1/sheetWidth;
	ftc.y = (TexCoord.y + min ( uint( sheetFrame/sheetWidth ), sheetHeight )) * 1/sheetHeight;
	if( flip > 0 ) ftc.x = 1.0 - ftc.x;
	col0 = texture2D ( tex0, ftc );
	col0 = col0 * colorMod;
	vec4 col1 = texture2D ( tex1, TexCoord );
	col0 = mix( col0, col1, col1.a * overa );

	gl_FragColor = vec4 ( col0.r * shading, col0.g * shading, col0.b * shading, col0.a );
}
