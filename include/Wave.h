#ifndef WAVE_MANAGE_H
#define WAVE_MANAGE_H

namespace Wave
{
	void update();
	void draw_enemies();

	unsigned total_enemies();
	void destroy();
	void init();
	void increment_total( int val = 1 );
	bool all_dead();
}
#endif //WAVE_MANAGE_H
