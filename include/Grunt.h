#ifndef GRUNT_H
#define GRUNT_H

#include <EvilBase.h>

namespace evil
{
	class Grunt: public EvilBase
	{
		public:
			Grunt();
			virtual ~Grunt();

			void draw();
			void update();
			void start();

			bool get_hurt( int damage, HurtCircle* from );
		protected:
			bool _attacking;
			char _moveUnit;
			float _lastTurn, _lastAttack;
			float _hitStun;
	};
}

#endif //GRUNT_H
