#ifndef WINDOW_LOADER_H
#define WINDOW_LOADER_H

class SDL_Window;
typedef unsigned int Uint32;
namespace wl//window loader
{
	struct SettingsPack
	{
		int sw, sh;
		Uint32 wflag;

		static const char SIZE_PREFIX = 'o';
		static const char FLAGS_PREFIX = 'f';
	};

	SettingsPack open_settings();
	bool save_settings(SDL_Window* dat);
}

#endif //WINDOW_LOADER_H
