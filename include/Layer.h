#ifndef LAYER_H
#define LAYER_H

namespace layer
{
	void reset();
	float top();
}

#endif //LAYER_H
