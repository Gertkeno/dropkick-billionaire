#ifndef EVIL_BASE_H
#define EVIL_BASE_H

#include <Actor.h>
#include <Animation.h>

namespace evil
{
	class EvilBase: public Actor
	{
		public:
			EvilBase();
			virtual ~EvilBase();

			virtual void update();
			virtual void draw() = 0;
			virtual void start();

			virtual bool get_hurt( int damage, HurtCircle* from );
			virtual void apply_force( const geo::point& offset );
			geo::point pos;
			virtual bool dead();
		protected:
			HurtCircle* _myHurt;
			bool _active;
			geo::point _knockback;
			//animation
			float _animationCycle;
			animation::Data _bodyParts;
			animation::Data (*_animeFun)(float);
	};
}
#endif //EVIL_BASE_H
