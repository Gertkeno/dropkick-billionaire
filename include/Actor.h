#ifndef ACTOR_H
#define ACTOR_H

class HurtCircle;
namespace geo{ class point; }

class Actor
{
	public:
		Actor( void );
		virtual ~Actor( void );

		virtual void update() = 0;
		virtual void draw() = 0;

		int health;
		virtual bool get_hurt( int damage, HurtCircle* from ) = 0;
		virtual void apply_force( const geo::point& offset ) = 0;
		static constexpr float GRAVITY = 12.0f;
	protected:

};

#endif //ACTOR_H
