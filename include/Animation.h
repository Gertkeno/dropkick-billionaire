#ifndef ANIMATION_H
#define ANIMATION_H

#include <Geometry.h>

namespace animation
{
	struct Data
	{
		geo::point head;
		geo::point leftHand;
		geo::point rightHand;
		geo::point leftFoot;
		geo::point rightFoot;
		float angle;

		Data operator+( const geo::point& );
		Data operator*( const geo::point& );
		Data operator*( const float& );
		Data();
	};

	void draw_anime( const geo::point& ani, float rot, int texture, float layer, geo::point size );

	//time assumed unit
	Data player_walk( float time );
	Data player_fall( float time );
}

#endif //ANIMATION_H
