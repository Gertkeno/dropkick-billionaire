#ifndef FRAME_TIMER_H
#define FRAME_TIMER_H

namespace FrameTime
{
	double get_pure();
	double get_mod();

	void set_modifier( double set );

	void update();
	unsigned since_update();
}

#endif //FRAME_TIMER_H
