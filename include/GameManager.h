#ifndef GAMEMANAGER_H
#define GAMEMANAGER_H

#include <Geometry.h>
typedef unsigned char uint8_t;
class HurtCircle;
class HitCircle;

namespace GameManager
{
	void init(void);
	void destroy(void);

	void update( void );
	void draw( void );
	void save_game( void );

	enum state: uint8_t
	{
		gmsPLAYING,
		/*gmsCUT_SCENE,
		  gmsNAME_ENTRY,
		  gmsTITLE_SCREEN,
		  gmsOPENING_CARDS,
		  gmsCONTROLLER_CONFIG,
		  gmsKEYBOARD_CONFIG,*/
		gmsQUITTING
	};

	void change_game_state( state ct );
	geo::point get_player_pos( void );
	state get_game_state();
};

#endif // GAMEMANAGER_H
