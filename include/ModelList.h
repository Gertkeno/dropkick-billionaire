#ifndef MODELLIST_H
#define MODELLIST_H

#include <Layer.h>

namespace gert
{
	class Wavefront;
	class Texture;
	class Shader;
	class Font;
}
typedef unsigned char uint8_t;

namespace model
{
	enum storage: uint8_t
	{
		DEBUG_SPHERE,
		PLANE,
		TOTAL
	};
}
extern gert::Wavefront* gModels;

namespace texture
{
	const char USE_LAST = -1;
	enum storage: uint8_t
	{
		RED_TEST,
		YELLOW_TEST,
		PLAYER_HEAD,
		PLAYER_FIST,
		PLAYER_FOOT,
		PLAYER_BODY,
		GRUNT_HEAD,
		GRUNT_FIST,
		GRUNT_FOOT,
		GRUNT_BODY,
		TOTAL
	};
}
extern gert::Texture* gTextures;

extern gert::Shader* gShaders;

extern gert::Font* gFont;

#endif // MODELLIST_H
