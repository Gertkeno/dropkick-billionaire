#ifndef HITCIRCLE_H
#define HITCIRCLE_H

#include <Geometry.h>

class Actor;

class HitCircle
{
	public:
		HitCircle();
		virtual ~HitCircle();

		void start( int damage, Actor* from, const geo::circle& pos );

		void update( void );

		geo::circle area;
		geo::point force;

		int damage;
		bool confirmHit;
		Actor* source;

		float maxTime;
		bool get_active( void );
		void kill( void );
	protected:
		float _lifeTime;
};

#endif // HITCIRCLE_H
