#ifndef MOVESET_PLAYER_H
#define MOVESET_PLAYER_H

#include <Action.h>
#include <Damage.h>

class Actor;
namespace moveset
{
	namespace normal
	{
		class DropKick: public Action
		{
			public:
				DropKick( const animation::Data& pos, Actor *owner );
				virtual ~DropKick();

				bool update();
				float get_timer();
			private:
				float gotHit;
				HitCircle *_lastHit;
				static constexpr float MAX_TIME = 0.7f;
		};

		class Punch: public Action
		{
			public:
				Punch( const animation::Data& pos, Actor* owner );
				virtual ~Punch();

				bool update();
				float get_timer();
			private:
				HitCircle *_lastHit;
				static constexpr float MAX_TIME = 0.3f;
		};
	}
}

#endif //MOVESET_PLAYER_H
