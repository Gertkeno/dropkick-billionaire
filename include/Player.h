#ifndef PLAYER_H
#define PLAYER_H

#include <Actor.h>
#include <Animation.h>

class Action;
class HurtCircle;

class Player: public Actor
{
	public:
		Player( void );
		virtual ~Player( void );

		virtual void update();
		virtual void draw();

		bool get_hurt( int damage, HurtCircle* from );
		void apply_force( const geo::point& offset );
		void remove_action();

		geo::point get_pos();
	protected:
		enum state_t: unsigned char
		{
			WALK,
			FALL,
			ACTION,
			DEBUG,
			TOTAL
		} state;
		geo::point _pos;	
		float _yspeed;
		float _offGroundTimer;
		void _change_state( state_t );
		//drawing
		animation::Data _bodyPart;
		float _animationCycle;
		bool _facingRight;

		Action *_currentAction;
		animation::Data (*_animeFunc)( float );
		HurtCircle *_myHurt;
};

#endif //PLAYER_H
