#ifndef DAMAGE_H
#define DAMAGE_H

#ifdef _GERT_DEBUG
#include <string>
#endif //_GERT_DEBUG
class HurtCircle;
class HitCircle;
class Actor;

namespace Damage
{
	HurtCircle* get_hurt_circ( Actor* target );
	HitCircle* get_hit_circ();

	void destroy();
	void clear_resources();
	void update();
	void check();
#ifdef _GERT_DEBUG
	void draw();
	std::string used_count();
#endif //_GERT_DEBUG
}

#endif //DAMAGE_H
