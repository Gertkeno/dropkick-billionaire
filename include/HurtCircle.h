#ifndef HURTCIRCLE_H
#define HURTCIRCLE_H

#include <Geometry.h>

struct Actor;
class HitCircle;

class HurtCircle
{
	public:
		HurtCircle();
		virtual ~HurtCircle();

		geo::circle area;
		HurtCircle operator=( const geo::circle& foo );
		Actor* target;

		bool check_collision( HitCircle* hitc );
	protected:

	private:
};

#endif // HURTCIRCLE_H
