#ifndef  GEOMETRY_H
#define GEOMETRY_H

namespace geo
{
	struct point
	{
		point();
		point( float x, float y );
		point( float b );
		
		float x, y;
		point normalize() const;
		point ratio() const;
		bool operator==( const point& comp ) const;
		point operator+( const point& ) const;
		point operator*( const point& ) const;
		point operator*( const float& ) const;
	};

	struct line
	{
		line(){};
		line( point a, point b );

		point a, b;

		point intersection( line pos ) const;
		point mid_point() const;
		float length() const;
		float slope() const;
		float offset() const;
		float angle() const;
		float get_height( float x ) const;
		
		float low_x() const;
		float high_x() const;
	};

	struct circle
	{
		circle();
		circle( float x, float y, float r );
		circle( point p, float r );
		float radius;
		point pos;

		line make_line( float radians ) const;

		circle operator=( const point& );
		bool operator==( const circle& ) const;//collision check
	};
}

#endif // GEOMETRY_H
