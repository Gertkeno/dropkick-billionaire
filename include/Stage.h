#ifndef STAGE_H
#define STAGE_H
#include <Geometry.h>

namespace Stage
{
	enum files
	{
		MOUNTAIN,
		TOTAL
	};
	bool load_stage( files type );
	void draw();
	//forces y value to match floor's
	float drape_floor( const geo::point& pos );
	geo::line drape_floor( const geo::point& one, const geo::point& two );
	geo::line drape_floor( const geo::line& pos );

	float wall_force( const geo::point& left, const geo::point& right );
	float wall_force( geo::line pos );
	float wall_force( const geo::circle& pos );

	geo::point stage_force( const geo::circle& pos );
}

#endif //STAGE_H
