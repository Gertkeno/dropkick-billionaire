#include <glm/gtc/matrix_transform.hpp>

namespace geo
{
	class line;
	class circle;

	glm::mat4 convert_line( const line&, const float& width );
	glm::mat4 convert_circle( const circle& );
}
