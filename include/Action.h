#ifndef ACTION_H
#define ACTION_H

#include <Animation.h>

class Actor;
class Action
{
	public:
		Action( void );
		virtual ~Action( void );

		virtual bool update() = 0;
		virtual void draw();
		virtual float get_timer() = 0;//return unit of timer
		virtual void kill();

		//use argument timer for multiple functions working asynchronously 
		bool get_active();
		bool hit_confirm();
		animation::Data position;
		float angle;
	protected:
		Actor *_owner;
		bool _active;
		float _timer;
		bool _hitConfirm;
};

#endif //ACTION_H
